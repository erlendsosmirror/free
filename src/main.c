/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/statvfs.h>
#include <simpleprint/simpleprint.h>

int os_utils (int a, int b, unsigned int c, int d)
{
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Structures
///////////////////////////////////////////////////////////////////////////////
typedef struct MemBankInfo
{
	unsigned int version;				// Always 1
	unsigned int baseaddress;			// What memory we allocate from
	unsigned int totalsize;				// Size of the memory
	unsigned int currentfree;			// How many free bytes we have now
	unsigned int lowestfree;			// Maximum utilization value
	unsigned int nallocationentries;	// How many entries we may have
} MemBankInfo;

///////////////////////////////////////////////////////////////////////////////
// Output printing
///////////////////////////////////////////////////////////////////////////////
void PrintValueAligned (int val, char *separator, int nalign)
{
	char str[32];
	_itoa (val, str);

	int len = strlen (str);

	if (len < nalign)
	{
		int mv = nalign - len;

		memmove (str+mv, str, len+1);

		for (int i = 0; i < mv; i++)
			str[i] = ' ';
	}

	SimplePrint (str);
	SimplePrint (separator);
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void BankInfo (int n, int extended)
{
	// Initialize structure
	MemBankInfo inf;
	memset (&inf, 0, sizeof (MemBankInfo));
	inf.version = 1;

	// Get data
	int ret = os_utils (2, n, (unsigned int) &inf, 0);

	// Check for errors
	if (ret)
	{
		SimplePrint ("Could not get bank info\n");
		return;
	}

	// If extended, print raw
	if (extended)
	{
		PrintValue ("bank    ", n);
		PrintValueHex ("baseaddr", inf.baseaddress);
		PrintValue ("totsize ", inf.totalsize);
		PrintValue ("free    ", inf.currentfree);
		PrintValue ("lowfree ", inf.lowestfree);
		PrintValue ("nentries", inf.nallocationentries);
	}
	else
	{
		// Print bank index
		PrintValueAligned (n, ":  ", 1);

		// Print total
		PrintValueAligned (inf.totalsize / 1024, " ", 4);

		// Print used
		PrintValueAligned ((inf.totalsize - inf.currentfree) / 1024, " ", 4);

		// Print free
		PrintValueAligned (inf.currentfree / 1024, " ", 4);

		// Print max
		PrintValueAligned ((inf.totalsize - inf.lowestfree) / 1024, "\n", 4);
	}
}

int GetFree (int extended)
{
	// Get number of banks
	int n = os_utils (1, 0, 0, 0);

	// Check for error
	if (n < 0 || n > 16)
	{
		SimplePrint ("Could not get nbanks\n");
		return 1;
	}

	// Print correct header
	if (!extended)
		SimplePrint (
			"All values in KiB\n"
			"   total used free max\n");

	// Loop through banks
	for (int i = 0; i < n; i++)
	{
		BankInfo (i, extended);

		if (extended && i != n-1)
			SimplePrint ("\n");
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Usage
///////////////////////////////////////////////////////////////////////////////
int PrintUsage ()
{
	SimplePrint ("Usage: free\n");
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// Main function
///////////////////////////////////////////////////////////////////////////////
int main (int argc, char **argv)
{
	if (argc == 1)
		return GetFree (0);
	else if (argc == 2 && !strcmp (argv[1], "-e"))
		return GetFree (1);
	else
		return PrintUsage ();
}
